#include <iostream> 
#include "f1.h" 
#include "f2.h"
using namespace std; 

int main() {
	int n1, n2;
	n1 = f1();
	n2 = f2();
	if (n1 == 2) {
		cout << "Good!\n" << endl;
	}
	else
		cout << "Not Good!\n" << endl;
	return 0;
}
